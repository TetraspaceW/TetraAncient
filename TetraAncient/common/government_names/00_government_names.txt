# For specific combinations of culture, religion and other such triggers
# Will pick the first valid one it finds in list

#Heir and consort titles are generally kept simple for clarity unless there is something special in particular that can be used.

egyptian_kingship = {
	rank = {
		1 = CITY_STATE
		2 = KINGDOM
		3 = EMPIRE
	}

	ruler_male = {
		1 = TEP
		2 = NESUT
		3 = PHARAOH
	}

	ruler_female = {
		1 = TEP_fem
		2 = NESUT_fem
		3 = PHARAOH_fem
	}
	
	consort_male = {
		1 = PRINCE_CONSORT
		2 = PRINCE_CONSORT
		3 = PRINCE_CONSORT
	}

	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}

	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
	}

	heir_female = {
		1 = HEIR_fem
		2 = HEIR_fem
		3 = HEIR_fem
	}

	trigger = {
		government = monarchy
		OR = {
			primary_culture = upper_egyptian
			primary_culture = lower_egyptian
			primary_culture = red_sea_egyptian
			primary_culture = oasis_egyptian
		}
	}
}

sumerian_kingship = {
	rank = {
		1 = CITY_STATE
		2 = KINGDOM
		3 = EMPIRE
	}

	ruler_male = {
		1 = ENSI
		2 = LUGAL
		3 = ENGAL_ENANA
	}

	ruler_female = {
		1 = ENSI_fem
		2 = LUGAL_fem
		3 = ENGAL_ENANA_fem
	}
	
	consort_male = {
		1 = PRINCE_CONSORT
		2 = PRINCE_CONSORT
		3 = PRINCE_CONSORT
	}

	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}

	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
	}

	heir_female = {
		1 = HEIR_fem
		2 = HEIR_fem
		3 = HEIR_fem
	}

	trigger = {
		OR = {
			government = monarchy
			government = theocracy
		}
		primary_culture = sumerian
	}
}

minoan_kingship = {
	rank = {
		1 = CITY_STATE
		2 = KINGDOM
		3 = EMPIRE
	}

	ruler_male = {
		1 = PETTY_KING
		2 = KING
		3 = EMPEROR
	}

	ruler_female = {
		1 = PRIEST_fem
		2 = HIGH_PRIEST_fem
		3 = SUPREME_PRIEST_fem
	}
	
	consort_male = {
		1 = PRINCE_CONSORT
		2 = PRINCE_CONSORT
		3 = PRINCE_CONSORT
	}

	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}

	heir_male = {
		1 = HEIR
		2 = HEIR
		3 = HEIR
	}

	heir_female = {
		1 = HEIR_fem
		2 = HEIR_fem
		3 = HEIR_fem
	}

	trigger = {
		government = monarchy
		primary_culture = minoan
	}
}