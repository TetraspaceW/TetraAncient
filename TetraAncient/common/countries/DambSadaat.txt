# Damb Sadaat

graphical_culture = westerngfx

color = { 174  252  235 }	

# Used for colonial nations
historical_idea_groups = {	
	expansion_ideas
	trade_ideas		
	plutocracy_ideas	
	economic_ideas
	quality_ideas	
	maritime_ideas	
	quantity_ideas	
	administrative_ideas
}