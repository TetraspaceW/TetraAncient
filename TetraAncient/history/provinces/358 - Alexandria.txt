#358 - Alexandria

capital = "Al Iskandirya"
trade_goods = spices
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes


discovered_by = western
discovered_by = muslim
discovered_by = ottoman
discovered_by = eastern
discovered_by = nomad_group
discovered_by = indian
discovered_by = east_african
add_province_triggered_modifier = coptic_alexandria
extra_cost = 8
center_of_trade = 1

2300.1.1 = {
	culture = lower_egyptian
	religion = kemetism
}