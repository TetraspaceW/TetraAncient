#850 - Tlaxcala





capital = "Tlaxcala"
trade_goods = sugar
hre = no 
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = mesoamerican


1519.1.1 = { discovered_by = SPA } # Hern�n Cort�s
1520.1.1 = {    remove_core = TLX} #Form a strong and long-lasting alliance with Spain
1525.1.1 = {  } #Dominican friars arrive, but Tlaxcala largely retain its own government under Spanish rule & protection (protectorate in-game)
1810.9.16 = {    } # Mexican War of Independence
1812.8.10 = {  } # Conquered by Jos� Mar�a Morelos
