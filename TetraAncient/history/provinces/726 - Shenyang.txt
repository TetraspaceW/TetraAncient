#726 - Liaodong






capital = "Shenyang"
trade_goods = gems #Youyan Jade
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes
 #the Nine Garrisons


discovered_by = chinese
discovered_by = nomad_group


1519.1.1 = { discovered_by = POR } # Tome Pires
1621.1.1 = {
	
	
	
	_new
} # The Later Jin Khanate
1625.1.1 = { capital = "Mukden" } # Renamed
1636.5.15 = {
	
	
	
	remove_core = MCH
} # The Qing Dynasty  
1657.1.1 = { capital = "Fengtianfu" } # Renamed
1662.1.1 = { remove_core = MNG } # The government in Taiwan surrendered
