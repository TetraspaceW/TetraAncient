#2072 - Bhakkar





capital = "Bhakkar"
trade_goods = cotton
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = indian
discovered_by = muslim 
discovered_by = ottoman
discovered_by = nomad_group
discovered_by = chinese

2700.1.1 = {
	base_manpower = 2
	base_production = 2
	base_tax = 2
	owner = KDI
	add_core = KDI
	culture = harappan
	religion = harappan_pagan
	}
	
2850.1.1 = {
	owner = INV
	add_core = INV
}