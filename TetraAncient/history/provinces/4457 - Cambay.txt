#4457 - Cambay/Khambhat

capital = "Khambhat"
trade_goods = cloth #Chintz
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

add_jains_or_burghers_effect = yes
discovered_by = east_african
discovered_by = indian
discovered_by = muslim 
discovered_by = ottoman
discovered_by = nomad_group
discovered_by = chinese
extra_cost = 24
center_of_trade = 3

2870.1.1 = {
	base_manpower = 2
	base_production = 2
	base_tax = 2
	owner = INV
	add_core = INV 
	culture = harappan
	religion = harappan_pagan
}