# NB: If changing the ID for this province, code (CPowerSpending::OnExecute) also has to change for achievement_tear_down_this_wall to work.
#50 - Brandenburg







base_tax = 1
base_production = 1
trade_goods = cloth
base_manpower = 1

capital = "Berlin-C�lln"
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman
extra_cost = 8
center_of_trade = 1


1539.1.1 = {  }
1594.1.1 = { fort_15th = no  } #Spandau
1648.1.1 = { fort_16th = no  } 
1650.1.1 = {  } 
1701.1.18 = {
	
	
	
	remove_core = BRA
	base_tax = 1 
	base_production = 1
	base_manpower = 1
} # Friedrich III becomes king of Prussia
1710.1.1 = {
	capital = "Berlin" #Merger of the twin cities.
}
1725.1.1 = {  base_tax = 1 base_production = 1 }
1750.1.1 = { base_manpower = 1 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1806.10.27 = {  }
1807.7.9 = {  } # The Second treaty of Tilsit
