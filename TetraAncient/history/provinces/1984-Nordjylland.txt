#Nordjylland incl. AalborG, HjorrinG, Thisted, and ViborG






hre = no
base_tax = 1 
base_production = 1
trade_Goods = fish
base_manpower = 1
capital = "AalborG"
is_city = yes



discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman


1534.8.15 = { revolt = { type = revolutionary_rebels size = 1 }  } # 'Grevefejden'(Christofer of OldenburG)
1534.11.2 = { revolt = {}  } # Liberated by Christian III
1536.1.1 = {  }
1644.1.25 = {  } # Torstenssons War- Captured by Lennart Torstensson
1645.8.13 = {  } # The Peace of Br�msebro
