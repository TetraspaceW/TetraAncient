# 197 Roussillon - Principal cities: Perpignan




capital = "Perpignan"
is_city = yes


hre = no
base_tax = 1 
base_production = 1
trade_goods = wine
base_manpower = 1






discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman

1412.6.29 = { revolt = { type = pretender_rebels size = 1 leader = "Jaume d'Urgell"}  } # Jaime de Urgell Uprising
1413.1.1 = { revolt = {}  }
1462.1.1 = {   } # Treaty of Bayonne: Aragon sells Roussillon to France
1493.1.19 = {   } # Treaty of Barcelona: Charles VIII restores Roussillon to Aragon
1516.1.23 = {
	
	
	 
} # Spain comes into existence, Carlos I rises to the throne
1583.1.1 = { fort_15th = no  }
1648.1.1 = { fort_16th = no  }
1659.10.28 = {    } # Peace of the Pyrennees
1674.8.10 = {  } # Spain invades Roussillon again
1678.9.19 = {  remove_core = SPA } # Treaty of Nijmegen (FRA-SPA)
1713.7.13 = { remove_core = ARA }
1715.1.1 = { fort_17th = no  }
1793.4.17 = {  } # Occupied by the Spanish army
1793.5.1 = {  }
