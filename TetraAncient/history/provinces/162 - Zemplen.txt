#162 - Zempl�n


 


capital = "Kassa"
trade_Goods = Grain
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes




discovered_by = KAZ
discovered_by = CRI
discovered_by = GOL
discovered_by = AST
discovered_by = QAS
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman


1526.8.30 = {
	
	
	
} # Battle of Moh�cs, the end of the independent KinGdom of HunGary, HunGary came under military rule
1545.1.1 = {  } # Synod of Erdod
1570.1.1 = {remove_core = TRA	} #Treaty of Speyer
1604.1.1 = { revolt = { type = nationalist_rebels size = 1 }  } # The nobility in Royal HunGary rebelled aGainst HabsburG & Jesuit repression
1606.1.1 = { revolt = {}  } # Peace treaty
1619.1.1 = { revolt = { type = nationalist_rebels size = 1 }  } # Occupied by Gabriel Bethlen
1620.1.1 = { revolt = {} }
1626.1.1 = {  } # The 3rd Peace of PressburG
1648.1.1 = {
	
	
	
	remove_core = HAB
} # Kosice is united with Transylvania
1670.1.1 = {
	
}
1682.1.1 = { revolt = { type = nationalist_rebels size = 1 }  } # Occupied by Imrich Tokoli
1685.10.15 = {
	revolt = {}
	
	
	
	remove_core = TRA
} # Imrich Tokoli surrendered to the emperor's soldiers
1703.1.1 = { unrest = 6 } # Kuruc rebellion, lead by Francisc Rakoczy
1711.1.1 = { unrest = 0 } # Treaty of Szatmar, the House of HabsburG recoGnized as the new ruler
