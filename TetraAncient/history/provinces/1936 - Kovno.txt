#Kovno





capital = "Kovno"
trade_goods = livestock
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes


discovered_by = KAZ
discovered_by = CRI
discovered_by = GOL
discovered_by = AST
discovered_by = QAS
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman


1550.1.1 = {  }
1569.7.1 = {
	
	
	
} # Union of Lublin
1589.1.1 = {  } #Birzai
1662.1.1 = { fort_16th = no  } #Birzai
1701.1.1 = {  } # Swedish occupation
1706.9.24 = {  } # Treaty of Altranstadt
1794.3.24 = { unrest = 6 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 }
1795.10.24 = {    } # Annexed by Russia, third partition
1812.6.28 = {  } # Occupied by French troops
1812.12.10 = {  }
