#601 - Sukhothai



_thai

capital = "Sukhothai"
trade_goods = chinaware
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes





discovered_by = chinese
discovered_by = indian


1438.1.1 = {	
	
} # Sukhothai is under Personal Union with Ayutthaya
1456.1.1 = {  } # Lan Na Invasion of Sukhothai
1474.1.1 = { remove_core = LNA }
1530.1.1 = { 
	
	remove_core = SUK
} # Completely annexed by Ayutthaya
1535.1.1 = { discovered_by = POR }
1767.4.1 = { unrest = 7 } # The fall of Ayutthaya
1774.1.1 = { unrest = 0 }
