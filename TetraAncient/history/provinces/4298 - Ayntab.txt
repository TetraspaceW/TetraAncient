#4398 - Ayntab

capital = "Ayntab"
trade_goods = wool
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman
discovered_by = nomad_group

2900.1.1 = {
	base_manpower = 2
	base_production = 2
	base_tax = 2
	owner = EBL
	add_core = EBL 
	culture = eblaite
	religion = semitic_pagan
}
