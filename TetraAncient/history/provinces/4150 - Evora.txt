#4150 - Alto Alentejo





capital = "�vora"
trade_goods = wine
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes


discovered_by = western
discovered_by = muslim
discovered_by = ottoman
discovered_by = eastern

1540.1.1 = {  } 
1640.1.1 = { unrest = 8 } # Revolt headed by John of Bragan�a
1640.12.1 = { unrest = 0 }
1642.1.1 = { fort_16th = no  }
1704.1.1 = {  } # War of the Spanish succession
1713.4.11 = {  } # Treaty of Utrecht
1735.1.1 = { fort_17th = no  } 
1801.5.26 = {  } # Invaded by Spanish troops
1801.6.6 = {  } # Treaty of Badajoz
1810.9.15 = {  } # Occupied by French troops
1811.4.5 = {  }
