#1228 - Dongola

capital = "Dunqula"

is_city = yes
trade_goods = grain
base_manpower = 1
hre = no
base_tax = 1
base_production = 1
discovered_by = east_african
discovered_by = muslim
discovered_by = ottoman

owner = NKR
add_core = NKR

culture = nubian
religion = animism