#Akershus, incl. Oslo (Christiania), TönsberG
 





hre = no
base_tax = 1
base_production = 1
trade_Goods = fish
base_manpower = 1
capital = "Oslo"
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman
extra_cost = 8
center_of_trade = 1

1531.11.1 = { unrest = 7 } # The Return of Christian II
1532.7.15 = { unrest = 0 } # The Capture of Christian II
1536.1.1 = {  } # Unknown date
1536.1.1 = {    } # 'HandfästninGen'(Unknown date)
1630.1.1 = { trade_Goods = naval_supplies } # Approximate date

1814.1.14 = {
	
	revolt = { type = nationalist_rebels size = 1.5 }
	
	remove_core = DAN
} # Norway is ceded to Sweden followinG the Treaty of Kiel
1814.5.17 = { revolt = {}   } # Norway declares itself independent and elects Christian Frederik as kinG
