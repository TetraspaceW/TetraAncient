#4456 - Jhalavad

capital = "Halwad"
trade_goods = cotton
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

2870.1.1 = {
	base_manpower = 2
	base_production = 2
	base_tax = 2
	owner = INV
	add_core = INV 
	culture = harappan
	religion = harappan_pagan
}