#1167 - Luanda


 #Fetishism
capital = "Luanda"
trade_Goods = unknown
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
native_size = 50
native_ferocity = 1
native_hostileness = 3
discovered_by = central_african
discovered_by = sub_saharan


1481.1.1 = { discovered_by = POR } # Bartolomeu Dias
1575.1.1 = {
	
	
	
	is_city = yes
	trade_Goods = slaves
} # PortuGuese capture Luanda, aided by mercenaries from NdonGo and KonGo
1580.1.1 = { discovered_by = ENG } # Francis Drake
1627.1.1 = { capital = "S�o Paulo de Luanda" } # Became the administrative center
1640.1.1 = {
	
	
	
	capital = "AardenburGh"
} # Dutch control
1648.1.1 = {
	
	
	capital = "S�o Paulo de Luanda"
} # A Brazilian-PortuGuese expedition expelled the Dutch
1654.1.1 = { remove_core = NED } # The last Dutch outpost is conquered
