#640 - Pontianak



capital = "Pontianak"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 60
native_ferocity = 5
native_hostileness = 7



discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = ottoman

1441.1.1 = {
	
	
	
	is_city = yes
	trade_goods = fish
}

1521.1.1 = { discovered_by = POR }

1777.1.1 = {
	
	
	
	is_city = yes
	trade_goods = gold
} # Lanfang Republic was founded by Chinese immigrants
