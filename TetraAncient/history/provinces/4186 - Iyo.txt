#4186 - Iyo





capital = "Yuzuki"
trade_goods = copper
hre = no
base_tax = 1
base_manpower = 1
base_production = 1
is_city = yes

discovered_by = chinese
discovered_by = MCH
discovered_by = KHA
discovered_by = OIR
discovered_by = QNG

1586.1.1 = {    remove_core = KNO } # Fall of the Kono Clan
1600.10.21 = {    remove_core = MRI }
1614.12.18 = {    remove_core = TKG } # Date-Uwajima Clan