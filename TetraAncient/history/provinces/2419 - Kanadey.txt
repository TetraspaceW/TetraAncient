#2419 - Kanadey





hre = no
base_tax = 1 
base_production = 1
trade_goods = livestock
base_manpower = 1
capital = "Kanadey"
is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman
discovered_by = nomad_group


1552.10.2 = {
	
	 
	 
	remove_core = KAZ
} # Russian conquest of Kazan
1641.1.1 = {   capital = "Syzran" }
1670.3.1 = { revolt = { type = anti_tax_rebels size = 2 name = "Stepan Razin" }  unrest = 6 } # Razin rebellion
1671.4.14 = { revolt = { }  unrest = 0 } # Razin captured
1773.9.13 = { revolt = { type = anti_tax_rebels size = 2 name = "Yemelyan Pugachev" }  unrest = 6 } # Pugachev rebellion
1775.2.1 = { revolt = {}  unrest = 0 } # Pugachev is captured
