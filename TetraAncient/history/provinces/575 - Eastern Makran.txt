#575 - Eastern Makran





capital = "Gwadar"
trade_goods = fish
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes

add_local_autonomy = 25


discovered_by = east_african
discovered_by = indian
discovered_by = muslim
discovered_by = ottoman
discovered_by = nomad_group

2850.1.1 = {
	base_manpower = 2
	base_production = 2
	base_tax = 2
	owner = INV
	add_core = INV
	culture = harappan
	religion = harappan_pagan
}