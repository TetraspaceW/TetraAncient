




capital = "Barus"
trade_goods = spices
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes




discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = ottoman

1405.1.1 = { discovered_by = MNG }

1509.1.1 = { discovered_by = POR }
