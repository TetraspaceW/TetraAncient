#4192 - Ugo





capital = "Akita"
trade_goods = fish
hre = no
base_tax = 1
base_manpower = 1
base_production = 1
is_city = yes

discovered_by = chinese
discovered_by = MCH
discovered_by = KHA
discovered_by = OIR
discovered_by = QNG
discovered_by = ANU

1542.1.1 = { discovered_by = POR }
1602.5.17 = {    remove_core = AKT } # The Satake clan moved to Akita
