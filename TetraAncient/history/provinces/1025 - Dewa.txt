#1025 - Dewa





capital = "Yamagata"
trade_goods = grain
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = chinese
discovered_by = MCH
discovered_by = KHA
discovered_by = OIR
discovered_by = QNG
discovered_by = ANU

1542.1.1 = { discovered_by = POR }
1598.1.10 = {    } # Uesugi Kagekatsu moved from Echigo to Aizu-Yonezawa
1622.8.21 = { remove_core = SBA }
