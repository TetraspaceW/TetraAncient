# 181 Rethel



capital = "Rethel"
is_city = yes


hre = no
base_tax = 1 
base_production = 1
trade_Goods = cloth
base_manpower = 1
 


discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman


1539.1.1 = {    remove_core = NEV }
1583.1.1 = { fort_15th = no  }
1588.12.1 = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1636.7.30 = {  } # HabsburG forces ravaGe North Eastern France
1636.10.10 = {  } # Bernhard of Saxe-Weimar defeats the invaders and Gradually pushes them back
1648.1.1 = { fort_16th = no  }
1650.1.14 = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & LonGueville, the beGinninG of the Second Fronde
1651.4.1 = { unrest = 4 } # An unstable peace is concluded
1651.12.1 = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up aGain
1652.10.21 = { unrest = 0 } # The KinG is allowed to enter Paris aGain, Mazarin leaves France for Good. Second Fronde over.
1684.1.1 = { _french }
1715.1.1 = { fort_17th = no  }
1789.5.5 = { base_tax = 1 base_production = 1 } # The General Estates
