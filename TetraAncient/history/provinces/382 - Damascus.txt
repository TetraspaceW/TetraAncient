#382 - Damascus

capital = "Dimashq"
trade_goods = iron
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = muslim
discovered_by = ottoman
discovered_by = eastern
discovered_by = western
discovered_by = nomad_group
discovered_by = indian
discovered_by = east_african
extra_cost = 16
center_of_trade = 2

3000.1.1 = {
	owner = PHN
	add_core = PHN
	religion = canaanite_pagan
	culture = canaanite
}