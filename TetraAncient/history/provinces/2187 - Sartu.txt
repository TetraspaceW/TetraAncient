# 2187 - Sartu






capital = "Sartu"
trade_goods = livestock
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = nomad_group


1624.1.1 = {
	
	
	
	remove_core = KRC
} # The Later Jin Khanate
1636.5.15 = {
	
	
	
	remove_core = MCH
} # The Qing Dynasty
1709.1.1 = { discovered_by = SPA }