#131 - ZaGorje





capital = "ZaGreb"
trade_Goods = cloth
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes


discovered_by = KAZ
discovered_by = CRI
discovered_by = GOL
discovered_by = AST
discovered_by = QAS
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman


1526.8.30 = {
	
	
	
} # Battle of Moh�cs
1573.1.20 = { revolt = { type = anti_tax_rebels size = 1 }  } # Peasant uprisinG aGainst HabsburG rule & the feudal system
1573.2.9 = { revolt = {}  } # Decisively defeated
1671.1.1 = { unrest = 5 } # Conspiracy aGainst HabsburG rule uncovered
1671.5.1 = { unrest = 0 }
1784.1.1 = { unrest = 7 } # Reforms to introduce German as the official lanGuaGe
1789.1.1 = { unrest = 0 } # Most of the unpopular reforms were cancelled
