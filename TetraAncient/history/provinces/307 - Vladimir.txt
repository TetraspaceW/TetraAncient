#307 - Vladimir


  


hre = no
base_tax = 1
base_production = 1
trade_goods = iron  
base_manpower = 1
capital = "Vladimir"
is_city = yes

discovered_by = GOL
discovered_by = KAZ
discovered_by = eastern
discovered_by = western
discovered_by = nomad_group

1450.1.1 = { discovered_by = SIB }
1503.4.1 = {
	
	
	
	remove_core = MOS 
}
1598.1.7 = { unrest = 5 } # "Time of troubles"
1613.3.3 = { unrest = 0 } # Order returned, Romanov dynasty
1711.1.1 = { base_tax = 1 base_production = 1 } # Governmental refoms and the absolutism
1767.1.1 = { base_tax = 1 base_production = 1 } # Legislative reform
1773.1.1 = { unrest = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { unrest = 0 } # Pugachev is captured
