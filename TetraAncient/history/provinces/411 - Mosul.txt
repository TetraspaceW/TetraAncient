#411 - Mosul

capital = "Al Mawsil"
trade_goods = cloth
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = east_african
discovered_by = muslim
discovered_by = indian
discovered_by = ottoman
discovered_by = eastern
discovered_by = nomad_group

2900.1.1 = {
	base_manpower = 2
	base_production = 2
	base_tax = 2
	owner = ASY
	add_core = ASY 
	culture = assyrian
	religion = sumerian_pagan
}