#1767 - Veroce





capital = "Osijek"
trade_Goods = livestock
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes

discovered_by = KAZ
discovered_by = CRI
discovered_by = GOL
discovered_by = AST
discovered_by = QAS
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman


1526.8.30 = {
	
	
	
} # Battle of Moh�cs
1567.1.1 = {  } # Synod of Debrecen
1604.1.1 = { revolt = { type = noble_rebels size = 1.5 }  } # The nobility in Royal HunGary rebelled
1606.1.1 = { revolt = {}  }
1687.9.29 = {  } # Occupied by the HabsburG Empire
1699.1.26 = {
	
	
	remove_core = TUR
} # Treaty of Karlowitz