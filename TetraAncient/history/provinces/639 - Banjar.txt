#639 - Banjar



capital = "Banjar"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 60
native_ferocity = 6
native_hostileness = 6

discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = ottoman

1444.1.1 = {
	
	
	
	is_city = yes
	trade_goods = cloth
} # Banjar Sultanate was founded

1521.1.1 = { discovered_by = POR }