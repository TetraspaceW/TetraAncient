# 2161 - Limchow






capital = "Limchow"
trade_goods = incense
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = chinese
discovered_by = nomad_group
discovered_by = indian

2620.1.1 = {
	culture = vietnamese
	religion = animism
	owner = XQU
	add_core = XQU
}
}