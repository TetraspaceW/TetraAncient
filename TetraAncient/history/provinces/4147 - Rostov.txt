#4147 - Rostov (includes Uglich)


  


hre = no
base_tax = 1
base_production = 1
trade_goods = grain  
base_manpower = 1
capital = "Rostov"
is_city = yes

# #Owns half of this province already
discovered_by = eastern
discovered_by = western
discovered_by = nomad_group
discovered_by = KAZ

1450.1.1 = { discovered_by = SIB }
1474.1.1 = { 
	
	
} #End of remaining half of the Rostov principality.
1503.4.1 = {
	
	
	
	remove_core = MOS
	remove_core = RSO
}
1560.1.1 = { base_tax = 1 
base_production = 1 } # Treasury reforms
1598.1.1 = { unrest = 5 } # "Time of troubles"
1613.1.1 = { unrest = 0 } # Order returned, Romanov dynasty
1711.1.1 = { base_tax = 1 base_production = 1 } # Governmental reforms and the absolutism
1767.1.1 = { base_tax = 1 base_production = 1 } # Legislative reform
