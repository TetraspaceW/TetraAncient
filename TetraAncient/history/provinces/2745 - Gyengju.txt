#2744 - Gyeongsang





capital = "Gyeongju"
trade_goods = tea
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes


discovered_by = chinese
discovered_by = nomad_group
extra_cost = 16
center_of_trade = 2


1542.1.1 = { discovered_by = POR }
1592.5.25 = {  } # Japanese invasion - Siege of Busan
1598.12.24 = {  }
1637.1.1 = {  } # Tributary of Qing China
1644.1.1 = {  remove_core = MNG } # Part of the Manchu empire
1645.7.1 = {  remove_core = MCH }
1653.1.1 = { discovered_by = NED } # Hendrick Hamel
