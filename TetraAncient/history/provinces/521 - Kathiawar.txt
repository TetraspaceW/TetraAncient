#521 - Kathiawar

capital = "Rajkot"
trade_goods = dyes
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes


discovered_by = indian
discovered_by = muslim
discovered_by = ottoman
discovered_by = nomad_group
discovered_by = chinese
discovered_by = east_african

2870.1.1 = {
	base_manpower = 2
	base_production = 2
	base_tax = 2
	owner = INV
	add_core = INV 
	culture = harappan
	religion = harappan_pagan
}