#4255 - Pronsk





hre = no
base_tax = 1
base_production = 1
trade_goods = iron   
base_manpower = 1
capital = "Pronsk"
is_city = yes

discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman
discovered_by = western
discovered_by = nomad_group


1521.8.1 = {
	
	 
	
	remove_core = RYA
} # Ryazan annexed by Vasily III
1598.1.7 = { unrest = 5 base_manpower = 1 } # "Time of troubles", peasantry brought into serfdom
1613.3.3 = { unrest = 0 } # Order returned, Romanov dynasty
1656.1.1 = { unrest = 4 } # Tatar revolt
1657.1.1 = { unrest = 0 }
1711.1.1 = { base_tax = 1 base_production = 1 } # Governmental reforms and the absolutism
1767.1.1 = { base_tax = 1 base_production = 1 } # Legislative reform
1778.1.1 = { capital = "Ryazan" } # Ryazan governorate formed, city renamed