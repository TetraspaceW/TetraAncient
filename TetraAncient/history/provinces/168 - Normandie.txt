# 168 Normandie - Principal cities: Caen


 
capital = "Caen"
is_city = yes


hre = no
base_tax = 1
base_production = 1
trade_goods = cloth
base_manpower = 1
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman




1415.10.25 = {  } # The Battle of Agincourt
1420.5.12 = {   } # Treaty of Troyes
1450.1.1 = {   } # Retaken by France
1465.8.15 = {   } # Treaty of Conflans
1466.3.6 = {   } # Louis XI reincorporates Normandy into the French crown
1475.8.29 = { remove_core = ENG } # Treaty of Picquigny
1583.1.1 = { fort_15th = no  }
1588.12.1 = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1631.1.1 = { unrest = 3 } # Region is restless
1633.1.1 = { unrest = 0 } 
1639.1.1 = { unrest = 3 }
1641.1.1 = { unrest = 0 }
1648.1.1 = { fort_16th = no  }
1715.1.1 = { fort_17th = no  }
1786.1.1 = { base_tax = 1 base_production = 1 } # The Eden Agreement
