#45 - L�beck




_saxon


base_tax = 1
base_production = 1
trade_goods = naval_supplies
base_manpower = 1

capital = "L�beck"
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman
extra_cost = 16
center_of_trade = 2

1531.1.1 = {  }
1550.1.1 = { base_tax = 1 
base_production = 1 } # Gradual shift in trading power from L�beck to Hamburg
1583.1.1 = { fort_15th = no  }
1600.1.1 = { base_tax = 1 
base_production = 1 shipyard = no } # Gradual shift in trading power from L�beck to Hamburg
1617.1.1 = { base_tax = 1 
base_production = 1 } # Stockholm commanded the Baltic Sea trade
1648.1.1 = { fort_16th = no  }
1715.1.1 = { fort_17th = no  } 
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1810.12.13 = {
	
	
	
} # Annexed by France
1814.4.11 = {
	
	
	remove_core = FRA
} # Napoleon abdicates undconditionally
