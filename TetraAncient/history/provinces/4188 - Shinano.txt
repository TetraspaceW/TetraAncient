#2017 - Shinano





capital = "Nagano"
trade_goods = livestock
hre = no
base_tax = 1
base_manpower = 1
base_production = 1
is_city = yes

discovered_by = chinese
discovered_by = MCH
discovered_by = KHA
discovered_by = OIR
discovered_by = QNG
discovered_by = ANU

1542.1.1   = { discovered_by = POR }
1542.6.1 = {  } # Takeda Shingen expands into Shinano
1550.7.1 = {   } # Fall of the Ogasawara Clan
1582.4.3 = {    remove_core = TKD }
1600.9.13 = {    remove_core = ODA }