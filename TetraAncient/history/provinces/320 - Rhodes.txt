#320 - Rhodes

 		# Knights of St. John Hospitaler of Jerusalem



capital = "Rhodes"
trade_goods = fish
hre = no
base_tax = 1 
base_production = 1
base_manpower = 1
is_city = yes


discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = nomad_group
discovered_by = ottoman


1522.12.21 = {	
	
	
	remove_core = KNI
} # Part of the Ottoman Empire
