#1750 - Al�cant

		#Mart� I of AraGon





hre = no
base_tax = 1
base_production = 1
trade_Goods = wine
base_manpower = 1 
capital = "Al�cant" 
is_city = yes



discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman


1516.1.23 = {
	
	
	
} # KinG Fernando dies, Carlos inherits AraGon and becames co-reGent of Castille
1519.7.1 = { unrest = 2 } # Revolt of the Germanies, which is at first moderate and appeased by the KinG
1521.9.11 = { unrest = 0 } # The AGermanats are soundly defeated in Oriola.
1609.9.22 = { unrest = 2 } # Decree for the expulsion of the morisques in Valencia. Morisque mutiny in the Alicante harbour.
1609.11.1 = { unrest = 0 } # The morisque mutiny is finally controlled. (economic consequences should be added)
1705.12.16 = {  } # Alicante joins the Austrian side in the War of the Spanish Succession
1707.5.8 = {  } # Alicante falls to the Borbonic troops
1713.7.13 = { remove_core = ARA }
1808.6.6 = { revolt = { type = pretender_rebels size = 0 }  }
1812.1.1 = { revolt = {}  }
1813.3.19 = { revolt = { type = pretender_rebels size = 0 }  }
1813.12.11 = { revolt = {}  }
